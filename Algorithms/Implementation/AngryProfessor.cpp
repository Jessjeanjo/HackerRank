//
//  AngryProfessor.cpp
//  Created by Jessica Joseph on 12/8/15.
//

#include <iostream>

using namespace std;


int main(){
	int testCaseNum, studentNum, cancelMin, arrivalTime, counter = 0;	
	
	cin >> testCaseNum;
		
	for (int i = 0; i < testCaseNum; i++){
		
		cin >> studentNum >> cancelMin;
		for (int j = 0; j < studentNum; j++){
			cin >> arrivalTime;
			if (arrivalTime <= 0)
				counter += 1;		
		
		}
	
		if (counter >= cancelMin)
			cout << "NO" << endl;
		else	
			cout << "YES" << endl;

		counter = 0;
	}

	return 0;
}
