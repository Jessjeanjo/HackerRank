//
//  Pangrams.cpp
//  Created by Jessica Joseph on 12/9/15.
//

#include <iostream>
#include <string>

using namespace std;


int main(){
	string testString;
	string firstVal, secondVal, thirdVal;

	getline(cin, testString);

	stringstream(testString) >> firstVal >> secondVal >> thirdVal;
	cout <<   firstVal << "==First" <<endl;
        cout <<   thirdVal << "==Third" <<endl;
        cout <<   secondVal << "==Second" <<endl;

}


/*

Problem Statement

Roy wanted to increase his typing speed for programming contests. So, his friend advised him to type the sentence "The quick brown fox jumps over the lazy dog" repeatedly, because it is a pangram. (Pangrams are sentences constructed by using every letter of the alphabet at least once.)

After typing the sentence several times, Roy became bored with it. So he started to look for other pangrams.

Given a sentence s, tell Roy if it is a pangram or not.

Input Format Input consists of a line containing s.

Constraints 
Length of s can be at most 103 (1≤|s|≤103) and it may contain spaces, lower case and upper case letters. Lower case and upper case instances of a letter are considered the same.

Output Format Output a line containing pangram if s is a pangram, otherwise output not pangram.


*/
